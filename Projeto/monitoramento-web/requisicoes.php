<?php


$con_string = "host=localhost port=5433 dbname=monitoramento user=postgres password=pg01";
$bdcon = pg_connect($con_string);


if($_REQUEST['act'] == 'recuperaJsonRegioes'){
	$regioes  = carregaRegioes();
	$qtdTipo  = carregaQtdTipoAtendimento();
	
	$completo = array();
	
	foreach( $regioes AS $chave => $dado){
		$regioes[$chave]['y']     = (int) $dado['y'];
		$completo[$chave]['id']   = $regioes[$chave]['drilldown'];
		$completo[$chave]['data'] = carregaEstadosByRegiao($regioes[$chave]['regcod']);
	}

	die(json_encode(array($regioes,$completo,$qtdTipo)));
}

if($_REQUEST['act'] == 'recuperaAtendimentosPorRegiao'){
	$dados = carregaAtendimentoByRegiao($_REQUEST['regcod']);
	die(json_encode($dados));
}

if($_REQUEST['act'] == 'recuperaAtendimentosPorEstado'){
	$dados = carregaAtendimentoByEstado($_REQUEST['estuf']);
	die(json_encode($dados));
}

if($_REQUEST['act'] == 'carregaEstados'){
	$dados = carregaEstados();
	die(json_encode($dados));
}

function carregaRegioes(){
	global $bdcon;
	
	$sql = "SELECT 
				*
			FROM 
				monitoramento.vw_regioes;"; 
					
	$result   = pg_exec($bdcon, $sql); 
	$consulta = @pg_fetch_all($result);
	return($consulta);
 }

function carregaEstadosByRegiao($regcod){
	global $bdcon;

	$sql = "SELECT 
				*
			FROM 
				monitoramento.vw_estados
			WHERE
				regcod = '{$regcod}';";

	$result   = pg_exec($bdcon, $sql); 
	$consulta = @pg_fetch_all($result);
	
	$retorno = array();
	foreach( $consulta AS $chave => $dado){
		$retorno[] = array( $consulta[$chave]['nome'] , (int) $consulta[$chave]['qtd'] );
	}
	
	return $retorno;
}


function carregaEstados(){
	global $bdcon;

	$sql = "SELECT 
				*
			FROM 
				monitoramento.vw_estados;";

	$result   = pg_exec($bdcon, $sql); 
	$consulta = @pg_fetch_all($result);
	return $consulta;
}

function carregaAtendimentoByRegiao($regcod){
	global $bdcon;

	$sql = "SELECT 
				*
			FROM 
				monitoramento.vw_atendimentos
			WHERE
				regcod = '{$regcod}';";

	$result   = pg_exec($bdcon, $sql); 
	$consulta = @pg_fetch_all($result);
	return $consulta;
}


function carregaAtendimentoByEstado($estuf = null){
	global $bdcon;

	$strEstuf = "";
	if($estuf){
		$strEstuf = " AND estuf = '{$estuf}' ";
	}
	
	$sql = "SELECT 
				*
			FROM 
				monitoramento.vw_atendimentos
			WHERE
				atostatus = 'A'
				{$strEstuf};";

	$result   = pg_exec($bdcon, $sql); 
	$consulta = @pg_fetch_all($result);
	return $consulta;
}

function carregaQtdTipoAtendimento(){
	global $bdcon;

	$sql = "SELECT 
				*
			FROM 
				monitoramento.vw_qtdportipo;"; 

	$result   = pg_exec($bdcon, $sql); 
	$consulta = @pg_fetch_all($result);
	
	$retorno = array();
	foreach( $consulta AS $chave => $dado){
		$retorno[] = array( $consulta[$chave]['tianome'] , (int) $consulta[$chave]['quantidade'] );
	}
	
	return $retorno;
}

pg_close($bdcon);