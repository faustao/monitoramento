-- Ponto Atendimento
DO
$do$
BEGIN 
FOR i IN 1..99 LOOP
   INSERT INTO monitoramento.pontoatendimento(
            poanome, poastatus, muncod)
    VALUES ('Ponto de Atendimento nº' || trunc(random() * 99999 + 1), 'A', (SELECT muncod FROM territorios.municipio OFFSET random()*5565 LIMIT 1));
END LOOP;
END
$do$;
