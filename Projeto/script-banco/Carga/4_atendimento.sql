INSERT INTO monitoramento.logcarga(lcaid, lcadtcarga) VALUES (1,now());

DO
$do$
BEGIN 
FOR i IN 1..1000 LOOP
	INSERT INTO monitoramento.atendimento(
		    atodtatendimento, atonotaatendimento, atotempoatendimento, 
		    ateid, tiaid, atostatus,lcaid)
	    VALUES ((SELECT NOW() - '1 year'::INTERVAL * RANDOM() * 5), trunc(random() * 10 + 1), trunc(random() * 30 + 1), 
		    (SELECT ateid FROM monitoramento.atendente OFFSET random()*(SELECT count(ateid) - 1 FROM monitoramento.atendente) LIMIT 1), (SELECT tiaid FROM monitoramento.tipoatendimento OFFSET random()*(SELECT count(tiaid) - 1 FROM monitoramento.tipoatendimento) LIMIT 1), 'A',1);
END LOOP;
END
$do$

