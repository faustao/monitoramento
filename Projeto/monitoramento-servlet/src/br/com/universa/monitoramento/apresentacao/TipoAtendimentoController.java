package br.com.universa.monitoramento.apresentacao;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import br.com.universa.arquitetura.monitoramento.apresentacao.AbstractController;
import br.com.universa.monitoramento.bean.TipoAtendimentoBean;
import br.com.universa.monitoramento.negocio.facade.TipoAtendimentoFacade;

@WebServlet("/tipoAtendimento")
public class TipoAtendimentoController extends
		AbstractController<TipoAtendimentoBean, TipoAtendimentoFacade> {

	private static final long serialVersionUID = 5502811753752229887L;

	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String acao = request.getParameter("acao");

		if (acao != null && acao.equals("listar")) {
			ArrayList<String> where = new ArrayList<String>();
			ArrayList<TipoAtendimentoBean> tiposAtendimento = getFacade()
					.listar(where);
			Gson gson = new Gson();
			String json = gson.toJson(tiposAtendimento);
			
			response.getWriter().write(json);
		}

	};

	@Override
	protected TipoAtendimentoBean doGetBean() {
		return new TipoAtendimentoBean();
	}

	@Override
	protected TipoAtendimentoFacade doGetFacade() {
		return new TipoAtendimentoFacade();
	}

}
