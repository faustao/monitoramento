package br.com.universa.monitoramento.apresentacao;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.google.gson.Gson;
import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

import br.com.universa.arquitetura.monitoramento.apresentacao.AbstractController;
import br.com.universa.monitoramento.bean.AtendimentoBean;
import br.com.universa.monitoramento.bean.CargaBean;
import br.com.universa.monitoramento.negocio.facade.AtendimentoFacade;
import br.com.universa.monitoramento.negocio.facade.CargaFacade;

@WebServlet("/upload")
public class CargaController extends AbstractController<CargaBean, CargaFacade> {
	private static final long serialVersionUID = -6719732484815380823L;

	private final String UPLOAD_DIRECTORY = "C:\\projects\\monitoramento\\Projeto";

	private AtendimentoBean atendimento;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// process only if its multipart content
		if (ServletFileUpload.isMultipartContent(request)) {
			try {
				List<FileItem> multiparts = new ServletFileUpload(
						new DiskFileItemFactory()).parseRequest(request);

				for (FileItem item : multiparts) {
					if (!item.isFormField()) {
						String name = new File(item.getName()).getName();
						item.write(new File(UPLOAD_DIRECTORY + File.separator
								+ name));
						Workbook workbook = Workbook.getWorkbook(new File(
								UPLOAD_DIRECTORY + File.separator + name));
						Sheet sheet = workbook.getSheet(0);

						int linhas = sheet.getRows();

						System.out
								.println("Iniciando a leitura da planilha XLS:");
						CargaFacade cargaFacade = new CargaFacade();
						CargaBean cargaBean = cargaFacade.salvar();
						for (int i = 0; i < linhas; i++) {
							atendimento = new AtendimentoBean();
							DateFormat formatter = new SimpleDateFormat(
									"yyyy-MM-dd");
							atendimento.setAtodtatendimento(formatter
									.parse(sheet.getCell(0, i).getContents()));
							atendimento.setAtonotaatendimento(sheet.getCell(1,
									i).getContents());
							atendimento.setAtotempoatendimento(sheet.getCell(2,
									i).getContents());
							atendimento.setAteid(Long.valueOf(sheet.getCell(3,
									i).getContents()));
							atendimento.setTiaid(Long.valueOf(sheet.getCell(4,
									i).getContents()));
							atendimento.setAtostatus(sheet.getCell(5, i)
									.getContents());
							atendimento.setLcaid(cargaBean.getId());

							AtendimentoFacade atendimentoFacade = new AtendimentoFacade();
							atendimentoFacade.salvar(atendimento);

						}

						workbook.close();
						System.out
								.println("Finalizando a leitura da planilha XLS");
					}
				}

				// File uploaded successfully
				request.setAttribute("message",
						"Importação realizada com sucesso!");
			} catch (Exception ex) {
				ex.printStackTrace();
				request.setAttribute("message", "File Upload Failed due to "
						+ ex);
			}

		} else {
			request.setAttribute("message",
					"Sorry this Servlet only handles file upload request");
		}

		request.getRequestDispatcher("/upload.jsp").forward(request, response);

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		String acao = request.getParameter("act");
		Gson gson = new Gson();
		
		if (acao != null && acao.equals("listar")) {
			CargaFacade cargaFacade = new CargaFacade();
			ArrayList<String> where =  new ArrayList<String>();
			ArrayList<CargaBean> cargas = cargaFacade.listar(where);
			
			for (CargaBean cargaBean : cargas) {
				SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
				cargaBean.setDatacarga(dt.format(cargaBean.getLcadtcarga()));
			}
			
			response.getWriter().write(gson.toJson(cargas));
		}
	};

	@Override
	protected CargaBean doGetBean() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected CargaFacade doGetFacade() {
		// TODO Auto-generated method stub
		return null;
	}
}
