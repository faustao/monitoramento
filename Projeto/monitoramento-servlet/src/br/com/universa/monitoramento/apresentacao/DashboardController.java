package br.com.universa.monitoramento.apresentacao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.universa.arquitetura.monitoramento.apresentacao.AbstractController;
import br.com.universa.monitoramento.bean.AtendimentosViewBean;
import br.com.universa.monitoramento.bean.EstadoViewBean;
import br.com.universa.monitoramento.bean.QtdAtendimentoPorTipoViewBean;
import br.com.universa.monitoramento.bean.RegioesViewBean;
import br.com.universa.monitoramento.bean.TipoAtendimentoBean;
import br.com.universa.monitoramento.negocio.facade.AtendimentosViewFacade;
import br.com.universa.monitoramento.negocio.facade.EstadoViewFacade;
import br.com.universa.monitoramento.negocio.facade.QtdAtendimentoPorTipoViewFacade;
import br.com.universa.monitoramento.negocio.facade.RegioesViewFacade;
import br.com.universa.monitoramento.negocio.facade.TipoAtendimentoFacade;

import com.google.gson.Gson;

@WebServlet("/dashboard")
public class DashboardController extends
		AbstractController<TipoAtendimentoBean, TipoAtendimentoFacade> {

	private static final long serialVersionUID = 5502811753752229887L;

	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setCharacterEncoding("UTF-8");
		String acao = request.getParameter("act");
		Gson gson = new Gson();

		if (acao != null && acao.equals("recuperaJsonRegioes")) {
			RegioesViewFacade regioesViewFacade = new RegioesViewFacade();
			QtdAtendimentoPorTipoViewFacade qtdAtendimentoPorTipoFacade = new QtdAtendimentoPorTipoViewFacade();

			ArrayList<String> where = new ArrayList<String>();
			ArrayList<RegioesViewBean> regioes = regioesViewFacade
					.listar(where);
			ArrayList<QtdAtendimentoPorTipoViewBean> qtdTipoRs = qtdAtendimentoPorTipoFacade
					.listar(where);

			ArrayList<ArrayList<Object>> qtdTipo = new ArrayList<ArrayList<Object>>();
			for (QtdAtendimentoPorTipoViewBean qtdAtendimentoPorTipoViewBean : qtdTipoRs) {
				ArrayList<Object> objects = new ArrayList<Object>();
				objects.add(qtdAtendimentoPorTipoViewBean.getTianome());
				objects.add(qtdAtendimentoPorTipoViewBean.getQuantidade());
				qtdTipo.add(objects);
			}

			ArrayList<HashMap<String, Object>> completo = new ArrayList<HashMap<String, Object>>();

			for (RegioesViewBean regioesViewBean : regioes) {
				HashMap<String, Object> value = new HashMap<String, Object>();
				value.put("id", regioesViewBean.getDrilldown());
				value.put("data", carregaEstadosByRegiao(regioesViewBean
						.getRegcod().toString()));
				completo.add(value);
			}

			ArrayList<Object> result = new ArrayList<Object>();
			result.add(regioes);
			result.add(completo);
			result.add(qtdTipo);

			String json = gson.toJson(result);

			response.getWriter().write(json);
		}

		if (acao != null && acao.equals("recuperaAtendimentosPorRegiao")) {
			ArrayList<String> where = new ArrayList<String>();
			String regcod = request.getParameter("regcod");
			AtendimentosViewFacade atendimentosViewFacade = new AtendimentosViewFacade();

			if (regcod != null) {
				where.add("regcod = '" + regcod + "'");
			}

			ArrayList<AtendimentosViewBean> atendimentosViewBeans = atendimentosViewFacade
					.listar(where);

			String json = gson.toJson(atendimentosViewBeans);

			response.getWriter().write(json);
		}

		if (acao != null && acao.equals("recuperaAtendimentosPorEstado")) {
			ArrayList<String> where = new ArrayList<String>();
			String estuf = request.getParameter("estuf");
			AtendimentosViewFacade atendimentosViewFacade = new AtendimentosViewFacade();

			if (estuf != null) {
				where.add("estuf = '" + estuf + "'");
			}

			ArrayList<AtendimentosViewBean> atendimentosViewBeans = atendimentosViewFacade
					.listar(where);

			String json = gson.toJson(atendimentosViewBeans);

			response.getWriter().write(json);
		}
		
		if (acao != null && acao.equals("carregaEstados")) {
			ArrayList<String> where = new ArrayList<String>();
			EstadoViewFacade estadoViewFacade = new EstadoViewFacade();

			ArrayList<EstadoViewBean> estadoViewBeans = estadoViewFacade
					.listar(where);

			String json = gson.toJson(estadoViewBeans);

			response.getWriter().write(json);
		}
		
		if (acao != null && acao.equals("enviarEmailRegiaoDeNotaBaixa")) {
			String destinatarios = request.getParameter("destinatarios");
			RegioesViewFacade regioesViewFacade = new RegioesViewFacade();
			regioesViewFacade.enviarEmailRegiaoDeNotaBaixa(destinatarios);
		}

	};

	private Object carregaEstadosByRegiao(String regcod) {
		EstadoViewFacade estadoViewFacade = new EstadoViewFacade();
		ArrayList<String> whereReg = new ArrayList<String>();
		whereReg.add("regcod = '" + regcod + "'");

		ArrayList<EstadoViewBean> estadoViewBeansRs = estadoViewFacade
				.listar(whereReg);

		ArrayList<ArrayList<Object>> estadoViewBeans = new ArrayList<ArrayList<Object>>();
		for (EstadoViewBean estadoViewBean : estadoViewBeansRs) {
			ArrayList<Object> objects = new ArrayList<Object>();
			objects.add(estadoViewBean.getNome());
			objects.add(estadoViewBean.getQtd());
			estadoViewBeans.add(objects);
		}

		return estadoViewBeans;
	}

	@Override
	protected TipoAtendimentoBean doGetBean() {
		return null;
	}

	@Override
	protected TipoAtendimentoFacade doGetFacade() {
		return null;
	}

}
