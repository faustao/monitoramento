package br.com.universa.monitoramento.negocio.facade;

import br.com.universa.arquitetura.monitoramento.negocio.facade.AbstractFacade;
import br.com.universa.monitoramento.bean.TipoAtendimentoBean;
import br.com.universa.monitoramento.negocio.model.TipoAtendimentoModel;

public class TipoAtendimentoFacade extends AbstractFacade<TipoAtendimentoBean, TipoAtendimentoModel> {

	@Override
	protected TipoAtendimentoModel doGetModel() {
		return new TipoAtendimentoModel();
	}

//	public ArrayList<TipoAtendimentoBean> listar(ArrayList<String> where) {
//		return getModel().listar(where);
//	}


}
