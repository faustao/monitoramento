package br.com.universa.monitoramento.negocio.model;

import br.com.universa.arquitetura.monitoramento.negocio.model.AbstractModel;
import br.com.universa.monitoramento.bean.AtendimentoBean;
import br.com.universa.monitoramento.dao.AtendimentoDAO;

public class AtendimentoModel extends AbstractModel<AtendimentoBean, AtendimentoDAO> {

	@Override
	protected AtendimentoDAO doGetDao() {
		try {
			return new AtendimentoDAO();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public AtendimentoBean salvar(AtendimentoBean atendimento) {
		return doGetDao().salvar(atendimento);
		
	}

}
