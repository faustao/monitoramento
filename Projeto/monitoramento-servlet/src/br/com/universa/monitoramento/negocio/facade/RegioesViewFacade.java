package br.com.universa.monitoramento.negocio.facade;

import br.com.universa.arquitetura.monitoramento.negocio.facade.AbstractFacade;
import br.com.universa.monitoramento.bean.RegioesViewBean;
import br.com.universa.monitoramento.negocio.model.RegioesViewModel;

public class RegioesViewFacade extends AbstractFacade<RegioesViewBean, RegioesViewModel> {

	public void enviarEmailRegiaoDeNotaBaixa(String destinatarios){
		getModel().enviarEmailRegiaoDeNotaBaixa(destinatarios);
	}
	
	@Override
	protected RegioesViewModel doGetModel() {
		return new RegioesViewModel();
	}

}
