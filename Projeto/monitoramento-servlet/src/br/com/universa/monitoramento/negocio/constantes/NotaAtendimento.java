package br.com.universa.monitoramento.negocio.constantes;

public enum NotaAtendimento {
	NOTA_BAIXA(3), NOTA_MEDIA(6), NOTA_ALTA(10);
	private final int valor;

	NotaAtendimento(int valorOpcao) {
		valor = valorOpcao;
	}

	public int getValor() {
		return valor;
	}
}
