package br.com.universa.monitoramento.negocio.model;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import br.com.universa.arquitetura.monitoramento.negocio.model.AbstractModel;
import br.com.universa.monitoramento.bean.CargaBean;
import br.com.universa.monitoramento.dao.CargaDao;

public class CargaModel extends AbstractModel<CargaBean, CargaDao> {

	@Override
	protected CargaDao doGetDao() {
		try {
			return new CargaDao();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public CargaBean salvar() {
		try {
			CargaBean cargaBean = new CargaBean();
			
			Calendar calendar = Calendar.getInstance();
			Date now = calendar.getTime();
			Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
			
			cargaBean.setLcadtcarga(currentTimestamp);
			
			CargaDao cargaDao;
			cargaDao = new CargaDao();
			return cargaDao.salvar(cargaBean);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
