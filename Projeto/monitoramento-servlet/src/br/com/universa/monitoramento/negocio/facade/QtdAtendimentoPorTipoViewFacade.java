package br.com.universa.monitoramento.negocio.facade;

import br.com.universa.arquitetura.monitoramento.negocio.facade.AbstractFacade;
import br.com.universa.monitoramento.bean.QtdAtendimentoPorTipoViewBean;
import br.com.universa.monitoramento.negocio.model.QtdAtendimentoPorTipoViewModel;

public class QtdAtendimentoPorTipoViewFacade extends AbstractFacade<QtdAtendimentoPorTipoViewBean, QtdAtendimentoPorTipoViewModel> {

	@Override
	protected QtdAtendimentoPorTipoViewModel doGetModel() {
		return new QtdAtendimentoPorTipoViewModel();
	}

}
