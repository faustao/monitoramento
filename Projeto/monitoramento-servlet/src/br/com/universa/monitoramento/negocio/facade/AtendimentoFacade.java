package br.com.universa.monitoramento.negocio.facade;

import br.com.universa.arquitetura.monitoramento.negocio.facade.AbstractFacade;
import br.com.universa.monitoramento.bean.AtendimentoBean;
import br.com.universa.monitoramento.negocio.model.AtendimentoModel;

public class AtendimentoFacade extends AbstractFacade<AtendimentoBean, AtendimentoModel> {

	@Override
	protected AtendimentoModel doGetModel() {
		return new AtendimentoModel();
	}

	public AtendimentoBean salvar(AtendimentoBean atendimento) {
		return doGetModel().salvar(atendimento);
	}

}
