package br.com.universa.monitoramento.negocio.constantes;

public enum TempoAtendimento {
	TEMPO_ALTO(7), TEMPO_BAIXO(4);
	private final int valor;

	TempoAtendimento(int valorOpcao) {
		valor = valorOpcao;
	}

	public int getValor() {
		return valor;
	}
}
