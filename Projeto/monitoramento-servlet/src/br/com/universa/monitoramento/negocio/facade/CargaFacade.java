package br.com.universa.monitoramento.negocio.facade;

import br.com.universa.arquitetura.monitoramento.negocio.facade.AbstractFacade;
import br.com.universa.monitoramento.bean.CargaBean;
import br.com.universa.monitoramento.negocio.model.CargaModel;

public class CargaFacade extends AbstractFacade<CargaBean, CargaModel> {

	@Override
	protected CargaModel doGetModel() {
		return new CargaModel();
	}

	public CargaBean salvar() {
		CargaModel cargaModel = new CargaModel();
		return cargaModel.salvar();
	}

}
