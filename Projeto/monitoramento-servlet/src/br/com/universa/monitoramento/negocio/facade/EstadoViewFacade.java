package br.com.universa.monitoramento.negocio.facade;

import br.com.universa.arquitetura.monitoramento.negocio.facade.AbstractFacade;
import br.com.universa.monitoramento.bean.EstadoViewBean;
import br.com.universa.monitoramento.negocio.model.EstadoViewModel;

public class EstadoViewFacade extends AbstractFacade<EstadoViewBean, EstadoViewModel> {

	@Override
	protected EstadoViewModel doGetModel() {
		return new EstadoViewModel();
	}

}
