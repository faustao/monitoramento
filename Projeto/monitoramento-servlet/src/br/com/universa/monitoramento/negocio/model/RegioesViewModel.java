package br.com.universa.monitoramento.negocio.model;

import java.util.ArrayList;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import br.com.universa.arquitetura.monitoramento.negocio.model.AbstractModel;
import br.com.universa.arquitetura.monitoramento.util.Email;
import br.com.universa.monitoramento.bean.AtendimentosViewBean;
import br.com.universa.monitoramento.bean.RegioesViewBean;
import br.com.universa.monitoramento.dao.RegioesViewDao;
import br.com.universa.monitoramento.negocio.constantes.NotaAtendimento;
import br.com.universa.monitoramento.negocio.constantes.TempoAtendimento;

public class RegioesViewModel extends
		AbstractModel<RegioesViewBean, RegioesViewDao> {

	public void enviarEmailRegiaoDeNotaBaixa(String destinatarios) {
		ArrayList<String> where = new ArrayList<String>();
		where.add("nota_media <= " + NotaAtendimento.NOTA_BAIXA);

		ArrayList<RegioesViewBean> regioesViewBeans = this.listar(where);
		RegioesViewBean regiaoEmail = null;

		if (regioesViewBeans.get(0) != null) {
			regiaoEmail = regioesViewBeans.get(0);
		}

		if (regiaoEmail != null) {
			AtendimentosViewModel atendimentosViewModel = new AtendimentosViewModel();
			ArrayList<String> whereAtendimentos = new ArrayList<String>();
			whereAtendimentos.add("regcod = '" + regiaoEmail.getRegcod() + "'");

			ArrayList<AtendimentosViewBean> atendimentosRegiao = atendimentosViewModel
					.listar(whereAtendimentos);

			Long tempoAtendimentoTotal = 0l;
			for (AtendimentosViewBean atendimentosViewBean : atendimentosRegiao) {
				tempoAtendimentoTotal += Integer.parseInt(atendimentosViewBean
						.getTempo());
			}

			Integer tempoMedioAtendimento = (int) (tempoAtendimentoTotal / regiaoEmail
					.getY());

			String conteudoEmail = "Prezado Gestor(a), </br></br>"
					+ "A regi�o "+ regiaoEmail.getName() +" encontra-se com a "
					+ "nota m�dia de Atendimento abaixo da m�dia (2.5). "
					+ "O Tempo m�dio de Atendimento desta regi�o � de "
					+ tempoMedioAtendimento + " minuto(s).";

			if (tempoMedioAtendimento >= Integer
					.parseInt(TempoAtendimento.TEMPO_ALTO.toString())) {
				conteudoEmail += "O que � bastante alto. </br>"
						+ "H� uma alta probabilidade de que a demora no atendimento "
						+ "esteja fazendo com que tantas notas negativas apare�am. "
						+ "V�rios funcion�rios nesta regi�o est�o reclamando de "
						+ "Travamento/Lentid�o no Sistema em hor�rio de pico, "
						+ "fazer um upgrade na infraestrutura da regi�o seria uma "
						+ "�tima solu��o para o problema.</br></br>";
			} else {
				RegioesViewBean regiaoComNotaAlta = getRegiaoComNotaAlta();
				conteudoEmail += "O que est� dentro dos pad�res da empresa. </br>"
						+ "Por�m falta algo mais. E que tal aprender um pouco com o time que est� ganhando?</br>"
						+ "A regi�o "
						+ regiaoComNotaAlta.getName()
						+ "est� com uma das notas mais altas do pa�s nos Atendimentos."
						+ "Uma boa conversa entre os colaboradoes da regi�o "
						+ "ou um Treinamento podem ser �timas solu��es "
						+ "para um aumento significativo no desempenho";
			}
			
			Email email = new Email();
			email.setAssunto("Notifica��o de Desempenho da regi�o " + regiaoEmail.getName());
			email.setConteudo(conteudoEmail);
			email.setDestinatarios(destinatarios);
			
			try {
				email.enviarEmail();
			} catch (AddressException e) {
				e.printStackTrace();
			} catch (MessagingException e) {
				e.printStackTrace();
			} 
		}

	}

	private RegioesViewBean getRegiaoComNotaAlta() {
		ArrayList<String> where = new ArrayList<String>();
		where.add("(nota_media >= " + NotaAtendimento.NOTA_MEDIA
				+ " AND nota_media <= " + NotaAtendimento.NOTA_ALTA + ")");

		ArrayList<RegioesViewBean> regioesViewBeans = this.listar(where);
		RegioesViewBean regiaoNComNotaAlta = null;

		if (regioesViewBeans.get(0) != null) {
			regiaoNComNotaAlta = regioesViewBeans.get(0);
		}

		return regiaoNComNotaAlta;
	}

	@Override
	protected RegioesViewDao doGetDao() {
		try {
			return new RegioesViewDao();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
