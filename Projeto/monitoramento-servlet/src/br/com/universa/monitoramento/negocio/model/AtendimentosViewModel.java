package br.com.universa.monitoramento.negocio.model;

import br.com.universa.arquitetura.monitoramento.negocio.model.AbstractModel;
import br.com.universa.monitoramento.bean.AtendimentosViewBean;
import br.com.universa.monitoramento.dao.AtendimentosViewDao;

public class AtendimentosViewModel extends
		AbstractModel<AtendimentosViewBean, AtendimentosViewDao> {

	@Override
	protected AtendimentosViewDao doGetDao() {
		try {
			return new AtendimentosViewDao();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
