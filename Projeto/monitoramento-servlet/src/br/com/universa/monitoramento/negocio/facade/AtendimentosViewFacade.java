package br.com.universa.monitoramento.negocio.facade;

import br.com.universa.arquitetura.monitoramento.negocio.facade.AbstractFacade;
import br.com.universa.monitoramento.bean.AtendimentosViewBean;
import br.com.universa.monitoramento.negocio.model.AtendimentosViewModel;

public class AtendimentosViewFacade extends AbstractFacade<AtendimentosViewBean, AtendimentosViewModel> {

	@Override
	protected AtendimentosViewModel doGetModel() {
		return new AtendimentosViewModel();
	}

}
