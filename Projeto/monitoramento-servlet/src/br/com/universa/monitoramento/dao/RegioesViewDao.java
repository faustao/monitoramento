package br.com.universa.monitoramento.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.universa.arquitetura.monitoramento.dao.AbstractDao;
import br.com.universa.monitoramento.bean.RegioesViewBean;

public class RegioesViewDao extends AbstractDao<RegioesViewBean> {

	public RegioesViewDao() throws ClassNotFoundException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected String doGetNomeTabela() {
		return "monitoramento.vw_regioes";
	}

	@Override
	protected String doGetNomeChavePrimaria() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String doGetNomeColunaStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ArrayList<String> doGetCamposTabela() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected RegioesViewBean doPopularBeanDaLista(ResultSet rs) {
		RegioesViewBean regioesViewBean = new RegioesViewBean();
		try {
			regioesViewBean.setName(rs.getString("name"));
			regioesViewBean.setY(rs.getInt("y"));
			regioesViewBean.setDrilldown(rs.getString("drilldown"));
			regioesViewBean.setRegcod(rs.getInt("regcod"));
			regioesViewBean.setNota(rs.getInt("nota"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return regioesViewBean;
	}

	@Override
	protected void doConfigurarPreparedStatement(
			PreparedStatement preparedStatement, RegioesViewBean bean)
			throws SQLException {
		// TODO Auto-generated method stub
		
	}

}
