package br.com.universa.monitoramento.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.universa.arquitetura.monitoramento.dao.AbstractDao;
import br.com.universa.arquitetura.monitoramento.util.ArrayListUtil;
import br.com.universa.monitoramento.bean.AtendimentoBean;

public class AtendimentoDAO extends AbstractDao<AtendimentoBean> {

	public AtendimentoDAO() throws ClassNotFoundException {
		super();
	}

	@Override
	protected String doGetNomeTabela() {
		return "monitoramento.atendimento";
	}

	@Override
	protected ArrayList<String> doGetCamposTabela() {
		return ArrayListUtil.explode(
			",",
			"atodtatendimento,atonotaatendimento,atotempoatendimento,ateid,tiaid,lcaid");
	}

	@Override
	protected void doConfigurarPreparedStatement(PreparedStatement ps, AtendimentoBean bean)
			throws SQLException {
		ps.setDate(1, new Date(bean.getAtodtatendimento().getTime()));
		ps.setString(2, bean.getAtonotaatendimento());
		ps.setString(3, bean.getAtotempoatendimento());
		ps.setLong(4, bean.getAteid());
		ps.setLong(5, bean.getTiaid());
		ps.setLong(6, bean.getLcaid());
	}

	protected String doGetNomeChavePrimaria() {
		return "atoid";
	}

	protected String doGetNomeColunaStatus() {
		return "atostatus";
	}

	protected AtendimentoBean doPopularBeanDaLista(ResultSet rs) {
		// TODO Auto-generated method stub
		return null;
	}

}
