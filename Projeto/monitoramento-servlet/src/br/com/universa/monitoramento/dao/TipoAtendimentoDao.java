package br.com.universa.monitoramento.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.universa.arquitetura.monitoramento.dao.AbstractDao;
import br.com.universa.arquitetura.monitoramento.util.ArrayListUtil;
import br.com.universa.monitoramento.bean.TipoAtendimentoBean;

public class TipoAtendimentoDao extends AbstractDao<TipoAtendimentoBean> {

	public TipoAtendimentoDao() throws ClassNotFoundException {
		super();
	}

	@Override
	protected String doGetNomeTabela() {
		return "monitoramento.tipoatendimento";
	}

	@Override
	protected String doGetNomeChavePrimaria() {
		return "tiaid";
	}

	@Override
	protected String doGetNomeColunaStatus() {
		return "tiastatus";
	}

	@Override
	protected ArrayList<String> doGetCamposTabela() {
		return ArrayListUtil.explode(",", "tianome");
	}

	@Override
	protected void doConfigurarPreparedStatement(PreparedStatement ps, TipoAtendimentoBean bean) throws SQLException {
		ps.setString(2, bean.getTianome());
	}

	@Override
	protected TipoAtendimentoBean doPopularBeanDaLista(ResultSet rs) {
		TipoAtendimentoBean tipoAtendimentoBean = new TipoAtendimentoBean();
		try {
			tipoAtendimentoBean.setId(rs.getInt("tiaid"));
			tipoAtendimentoBean.setTianome(rs.getString("tianome"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return tipoAtendimentoBean;
	}

}
