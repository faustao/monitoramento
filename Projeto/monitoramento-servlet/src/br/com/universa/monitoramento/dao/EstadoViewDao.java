package br.com.universa.monitoramento.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.universa.arquitetura.monitoramento.dao.AbstractDao;
import br.com.universa.monitoramento.bean.EstadoViewBean;

public class EstadoViewDao extends AbstractDao<EstadoViewBean> {

	public EstadoViewDao() throws ClassNotFoundException {
		super();
	}

	@Override
	protected String doGetNomeTabela() {
		return "monitoramento.vw_estados";
	}

	@Override
	protected String doGetNomeChavePrimaria() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String doGetNomeColunaStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ArrayList<String> doGetCamposTabela() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected EstadoViewBean doPopularBeanDaLista(ResultSet rs) {
		EstadoViewBean estadoViewBean = new EstadoViewBean();
		
		try {
			estadoViewBean.setEstuf(rs.getString("estuf"));
			estadoViewBean.setNome(rs.getString("nome"));
			estadoViewBean.setNota(rs.getInt("nota"));
			estadoViewBean.setQtd(rs.getInt("qtd"));
			estadoViewBean.setRegcod(rs.getInt("regcod"));
			estadoViewBean.setTempo(rs.getInt("tempo"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return estadoViewBean;
	}

	@Override
	protected void doConfigurarPreparedStatement(
			PreparedStatement preparedStatement, EstadoViewBean bean)
			throws SQLException {
		// TODO Auto-generated method stub

	}

}
