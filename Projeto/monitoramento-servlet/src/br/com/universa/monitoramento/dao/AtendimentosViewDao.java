package br.com.universa.monitoramento.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.universa.arquitetura.monitoramento.dao.AbstractDao;
import br.com.universa.monitoramento.bean.AtendimentosViewBean;

public class AtendimentosViewDao extends AbstractDao<AtendimentosViewBean> {

	public AtendimentosViewDao() throws ClassNotFoundException {
		super();
	}

	@Override
	protected String doGetNomeTabela() {
		return "monitoramento.vw_atendimentos";
	}

	@Override
	protected String doGetNomeChavePrimaria() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String doGetNomeColunaStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ArrayList<String> doGetCamposTabela() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected AtendimentosViewBean doPopularBeanDaLista(ResultSet rs) {
		AtendimentosViewBean atendimentosViewBean = new AtendimentosViewBean();
		try {
			atendimentosViewBean.setAteid(rs.getInt("ateid"));
			atendimentosViewBean.setAtenome(rs.getString("atenome"));
			atendimentosViewBean.setAtodtatendimento(rs.getTimestamp("atodtatendimento"));
			atendimentosViewBean.setAtoid(rs.getString("atoid"));
			atendimentosViewBean.setAtostatus(rs.getString("atostatus"));
			atendimentosViewBean.setDataatendimento(rs.getString("dataatendimento"));
			atendimentosViewBean.setEstdescricao(rs.getString("estdescricao"));
			atendimentosViewBean.setEstuf(rs.getString("estuf"));
			atendimentosViewBean.setLcaid(rs.getInt("lcaid"));
			atendimentosViewBean.setMundescricao(rs.getString("mundescricao"));
			atendimentosViewBean.setNota(rs.getString("nota"));
			atendimentosViewBean.setPoanome(rs.getString("poanome"));
			atendimentosViewBean.setRegcod(rs.getString("regcod"));
			atendimentosViewBean.setTempo(rs.getString("tempo"));
			atendimentosViewBean.setTiaid(rs.getInt("tiaid"));
			atendimentosViewBean.setTianome(rs.getString("tianome"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return atendimentosViewBean;
	}

	@Override
	protected void doConfigurarPreparedStatement(
			PreparedStatement preparedStatement, AtendimentosViewBean bean)
			throws SQLException {
		// TODO Auto-generated method stub
		
	}

}
