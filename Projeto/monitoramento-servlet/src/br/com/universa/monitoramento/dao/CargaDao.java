package br.com.universa.monitoramento.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.universa.arquitetura.monitoramento.dao.AbstractDao;
import br.com.universa.arquitetura.monitoramento.util.ArrayListUtil;
import br.com.universa.monitoramento.bean.CargaBean;

public class CargaDao extends AbstractDao<CargaBean> {

	public CargaDao() throws ClassNotFoundException {
		super();
	}

	@Override
	protected String doGetNomeTabela() {
		return "monitoramento.logcarga";
	}

	@Override
	protected String doGetNomeChavePrimaria() {
		return "lcaid";
	}

	@Override
	protected String doGetNomeColunaStatus() {
		return "lcastatus";
	}

	@Override
	protected ArrayList<String> doGetCamposTabela() {
		return ArrayListUtil.explode(",", "lcadtcarga");
	}

	@Override
	protected CargaBean doPopularBeanDaLista(ResultSet rs) {
		CargaBean cargaBean = new CargaBean();
		try {
			cargaBean.setId(rs.getLong("lcaid"));
			cargaBean.setLcadtcarga(rs.getTimestamp("lcadtcarga"));
			cargaBean.setStatus(rs.getString("lcastatus"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return cargaBean;
	}

	@Override
	protected void doConfigurarPreparedStatement(
			PreparedStatement preparedStatement, CargaBean bean)
			throws SQLException {
		preparedStatement.setTimestamp(1, bean.getLcadtcarga());
	}

}
