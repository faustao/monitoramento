package br.com.universa.monitoramento.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.universa.arquitetura.monitoramento.dao.AbstractDao;
import br.com.universa.arquitetura.monitoramento.util.ArrayListUtil;
import br.com.universa.monitoramento.bean.PontoAtendimentoBean;

public class PontoAtendimentoDAO extends AbstractDao<PontoAtendimentoBean> {

	public PontoAtendimentoDAO() throws ClassNotFoundException {
		super();
	}

	@Override
	protected String doGetNomeTabela() {
		return "pontoatendimento";
	}

	@Override
	protected String doGetNomeChavePrimaria() {
		return "poaid";
	}

	@Override
	protected String doGetNomeColunaStatus() {
		return "poastatus";
	}

	@Override
	protected ArrayList<String> doGetCamposTabela() {
		return ArrayListUtil.explode(",","poanome,muncod");
	}

	@Override
	protected void doConfigurarPreparedStatement(PreparedStatement ps, PontoAtendimentoBean bean) throws SQLException {
		ps.setString(2, bean.getPoanome());
		ps.setLong(3, bean.getMuncod());
	}

	@Override
	protected PontoAtendimentoBean doPopularBeanDaLista(ResultSet rs) {
		// TODO Auto-generated method stub
		return null;
	}

}
