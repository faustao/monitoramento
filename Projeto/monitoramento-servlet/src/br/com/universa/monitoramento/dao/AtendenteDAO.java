package br.com.universa.monitoramento.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.universa.arquitetura.monitoramento.dao.AbstractDao;
import br.com.universa.arquitetura.monitoramento.util.ArrayListUtil;
import br.com.universa.monitoramento.bean.AtendenteBean;

public class AtendenteDAO extends AbstractDao<AtendenteBean> {

	public AtendenteDAO() throws ClassNotFoundException {
		super();
	}

	@Override
	protected String doGetNomeTabela() {
		return "atendente";
	}

	@Override
	protected ArrayList<String> doGetCamposTabela() {
		return ArrayListUtil.explode(",", "atenome,poaid");
	}

	@Override
	protected void doConfigurarPreparedStatement(PreparedStatement ps,
			AtendenteBean bean) throws SQLException {
		ps.setString(2, bean.getAtenome());
		ps.setLong(3, bean.getPoaid());
	}

	@Override
	protected String doGetNomeChavePrimaria() {
		return "ateid";
	}

	@Override
	protected String doGetNomeColunaStatus() {
		return "atestatus";
	}

	@Override
	protected AtendenteBean doPopularBeanDaLista(ResultSet rs) {
		// TODO Auto-generated method stub
		return null;
	}
}
