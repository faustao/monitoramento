package br.com.universa.monitoramento.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.universa.arquitetura.monitoramento.dao.AbstractDao;
import br.com.universa.monitoramento.bean.QtdAtendimentoPorTipoViewBean;

public class QtdAtendimentoPorTipoViewDao extends AbstractDao<QtdAtendimentoPorTipoViewBean> {

	public QtdAtendimentoPorTipoViewDao() throws ClassNotFoundException {
		super();
	}

	@Override
	protected String doGetNomeTabela() {
		// TODO Auto-generated method stub
		return "monitoramento.vw_qtdportipo";
	}

	@Override
	protected String doGetNomeChavePrimaria() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String doGetNomeColunaStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ArrayList<String> doGetCamposTabela() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected QtdAtendimentoPorTipoViewBean doPopularBeanDaLista(ResultSet rs) {
		QtdAtendimentoPorTipoViewBean atendimentoPorTipoViewBean = new QtdAtendimentoPorTipoViewBean();
		try {
			atendimentoPorTipoViewBean.setTianome(rs.getString("tianome"));
			atendimentoPorTipoViewBean.setQuantidade(rs.getInt("quantidade"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return atendimentoPorTipoViewBean;
	}

	@Override
	protected void doConfigurarPreparedStatement(
			PreparedStatement preparedStatement,
			QtdAtendimentoPorTipoViewBean bean) throws SQLException {
		// TODO Auto-generated method stub
		
	}

}
