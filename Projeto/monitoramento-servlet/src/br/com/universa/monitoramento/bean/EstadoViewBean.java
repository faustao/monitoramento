package br.com.universa.monitoramento.bean;

import br.com.universa.arquitetura.monitoramento.bean.AbstractBean;

public class EstadoViewBean extends AbstractBean {
	private String nome;
	private Integer qtd;
	private Integer nota;
	private Integer tempo;
	private Integer regcod;
	private String estuf;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getQtd() {
		return qtd;
	}

	public void setQtd(Integer qtd) {
		this.qtd = qtd;
	}

	public Integer getNota() {
		return nota;
	}

	public void setNota(Integer nota) {
		this.nota = nota;
	}

	public Integer getTempo() {
		return tempo;
	}

	public void setTempo(Integer tempo) {
		this.tempo = tempo;
	}

	public Integer getRegcod() {
		return regcod;
	}

	public void setRegcod(Integer regcod) {
		this.regcod = regcod;
	}

	public String getEstuf() {
		return estuf;
	}

	public void setEstuf(String estuf) {
		this.estuf = estuf;
	}
}
