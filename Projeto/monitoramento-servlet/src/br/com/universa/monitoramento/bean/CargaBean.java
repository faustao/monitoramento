package br.com.universa.monitoramento.bean;

import java.sql.Timestamp;

import br.com.universa.arquitetura.monitoramento.bean.AbstractBean;

public class CargaBean extends AbstractBean{
	private Timestamp lcadtcarga;
	private String datacarga;
	
	public Timestamp getLcadtcarga() {
		return lcadtcarga;
	}
	public void setLcadtcarga(Timestamp lcadtcarga) {
		this.lcadtcarga = lcadtcarga;
	}
	public String getDatacarga() {
		return datacarga;
	}
	public void setDatacarga(String datacarga) {
		this.datacarga = datacarga;
	}
	
	
}
