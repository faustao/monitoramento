package br.com.universa.monitoramento.bean;

import br.com.universa.arquitetura.monitoramento.bean.AbstractBean;

public class QtdAtendimentoPorTipoViewBean extends AbstractBean {
	private String tianome;
	private Integer quantidade;

	public String getTianome() {
		return tianome;
	}

	public void setTianome(String tianome) {
		this.tianome = tianome;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

}
