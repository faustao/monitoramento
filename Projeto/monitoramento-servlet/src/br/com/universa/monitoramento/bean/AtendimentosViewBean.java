package br.com.universa.monitoramento.bean;

import java.sql.Timestamp;

import br.com.universa.arquitetura.monitoramento.bean.AbstractBean;

public class AtendimentosViewBean extends AbstractBean {
	private String dataatendimento;
	private String nota;
	private String tempo;
	private String atenome;
	private String poanome;
	private String mundescricao;
	private String estdescricao;
	private String tianome;
	private String atoid;
	private Timestamp atodtatendimento;
	private Integer ateid;
	private Integer tiaid;
	private String atostatus;
	private Integer lcaid;
	private String regcod;
	private String estuf;

	public String getDataatendimento() {
		return dataatendimento;
	}

	public void setDataatendimento(String dataatendimento) {
		this.dataatendimento = dataatendimento;
	}

	public String getNota() {
		return nota;
	}

	public void setNota(String nota) {
		this.nota = nota;
	}

	public String getTempo() {
		return tempo;
	}

	public void setTempo(String tempo) {
		this.tempo = tempo;
	}

	public String getAtenome() {
		return atenome;
	}

	public void setAtenome(String atenome) {
		this.atenome = atenome;
	}

	public String getPoanome() {
		return poanome;
	}

	public void setPoanome(String poanome) {
		this.poanome = poanome;
	}

	public String getMundescricao() {
		return mundescricao;
	}

	public void setMundescricao(String mundescricao) {
		this.mundescricao = mundescricao;
	}

	public String getEstdescricao() {
		return estdescricao;
	}

	public void setEstdescricao(String estdescricao) {
		this.estdescricao = estdescricao;
	}

	public String getTianome() {
		return tianome;
	}

	public void setTianome(String tianome) {
		this.tianome = tianome;
	}

	public String getAtoid() {
		return atoid;
	}

	public void setAtoid(String atoid) {
		this.atoid = atoid;
	}

	public Timestamp getAtodtatendimento() {
		return atodtatendimento;
	}

	public void setAtodtatendimento(Timestamp atodtatendimento) {
		this.atodtatendimento = atodtatendimento;
	}

	public Integer getAteid() {
		return ateid;
	}

	public void setAteid(Integer ateid) {
		this.ateid = ateid;
	}

	public Integer getTiaid() {
		return tiaid;
	}

	public void setTiaid(Integer tiaid) {
		this.tiaid = tiaid;
	}

	public String getAtostatus() {
		return atostatus;
	}

	public void setAtostatus(String atostatus) {
		this.atostatus = atostatus;
	}

	public Integer getLcaid() {
		return lcaid;
	}

	public void setLcaid(Integer lcaid) {
		this.lcaid = lcaid;
	}

	public String getRegcod() {
		return regcod;
	}

	public void setRegcod(String regcod) {
		this.regcod = regcod;
	}

	public String getEstuf() {
		return estuf;
	}

	public void setEstuf(String estuf) {
		this.estuf = estuf;
	}

}
