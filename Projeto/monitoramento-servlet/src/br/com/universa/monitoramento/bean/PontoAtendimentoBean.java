package br.com.universa.monitoramento.bean;

import br.com.universa.arquitetura.monitoramento.bean.AbstractBean;

public class PontoAtendimentoBean extends AbstractBean {
	private String poanome;
	private Long muncod;

	public String getPoanome() {
		return poanome;
	}
	public void setPoanome(String poanome) {
		this.poanome = poanome;
	}
	public Long getMuncod() {
		return muncod;
	}
	public void setMuncod(Long muncod) {
		this.muncod = muncod;
	}
}
