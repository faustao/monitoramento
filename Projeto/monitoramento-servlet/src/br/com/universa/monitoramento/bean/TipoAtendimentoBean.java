package br.com.universa.monitoramento.bean;

import br.com.universa.arquitetura.monitoramento.bean.AbstractBean;

public class TipoAtendimentoBean extends AbstractBean {
	private String tianome;

	public String getTianome() {
		return tianome;
	}
	public void setTianome(String tianome) {
		this.tianome = tianome;
	}
}
