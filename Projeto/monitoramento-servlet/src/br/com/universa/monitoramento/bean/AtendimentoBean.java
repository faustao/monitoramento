package br.com.universa.monitoramento.bean;

import java.util.Date;

import br.com.universa.arquitetura.monitoramento.bean.AbstractBean;

public class AtendimentoBean extends AbstractBean {
	private Date atodtatendimento;
	private String atonotaatendimento;
	private String atotempoatendimento;
	private Long ateid;
	private Long tiaid;
	private Long lcaid;
	private String atostatus;
	
	public Date getAtodtatendimento() {
		return atodtatendimento;
	}
	public void setAtodtatendimento(Date atodtatendimento) {
		this.atodtatendimento = atodtatendimento;
	}
	public String getAtonotaatendimento() {
		return atonotaatendimento;
	}
	public void setAtonotaatendimento(String atonotaatendimento) {
		this.atonotaatendimento = atonotaatendimento;
	}
	public String getAtotempoatendimento() {
		return atotempoatendimento;
	}
	public void setAtotempoatendimento(String atotempoatendimento) {
		this.atotempoatendimento = atotempoatendimento;
	}
	public Long getAteid() {
		return ateid;
	}
	public void setAteid(Long ateid) {
		this.ateid = ateid;
	}
	public Long getTiaid() {
		return tiaid;
	}
	public void setTiaid(Long tiaid) {
		this.tiaid = tiaid;
	}
	public Long getLcaid() {
		return lcaid;
	}
	public void setLcaid(Long lcaid) {
		this.lcaid = lcaid;
	}
	public String getAtostatus() {
		return atostatus;
	}
	public void setAtostatus(String atostatus) {
		this.atostatus = atostatus;
	}
}
