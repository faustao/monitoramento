package br.com.universa.monitoramento.bean;

import br.com.universa.arquitetura.monitoramento.bean.AbstractBean;

public class RegioesViewBean extends AbstractBean {
	private String name;

	private Integer y;

	private String drilldown;

	private Integer regcod;

	private Integer nota;

	private Integer nota_media;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public String getDrilldown() {
		return drilldown;
	}

	public void setDrilldown(String drilldown) {
		this.drilldown = drilldown;
	}

	public Integer getRegcod() {
		return regcod;
	}

	public void setRegcod(Integer regcod) {
		this.regcod = regcod;
	}

	public Integer getNota() {
		return nota;
	}

	public void setNota(Integer nota) {
		this.nota = nota;
	}

	public Integer getNota_media() {
		return nota_media;
	}

	public void setNota_media(Integer nota_media) {
		this.nota_media = nota_media;
	}

}
