package br.com.universa.monitoramento.bean;

import br.com.universa.arquitetura.monitoramento.bean.AbstractBean;

public class AtendenteBean extends AbstractBean{
	private String atenome;
	private long poaid;
	
	public String getAtenome() {
		return atenome;
	}
	public void setAtenome(String atenome) {
		this.atenome = atenome;
	}
	public long getPoaid() {
		return poaid;
	}
	public void setPoaid(long poaid) {
		this.poaid = poaid;
	}
}
