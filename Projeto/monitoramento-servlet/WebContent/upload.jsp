<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Importação de Xls</title>
        <script type="text/javascript" src="scripts/jquery-1.9.1.js"></script>	
		<script src="scripts/jquery.easing.1.3.js"></script>		
		<!-- Bootstrap -->
		<link rel="stylesheet" href="estilo/bootstrap.min.css">
		<script src="scripts/bootstrap.min.js"></script>
		<script src="scripts/highcharts.js"></script>
		<script src="scripts/highcharts-more.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Archivo+Narrow:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="estilo/jquery-ui.css">
        <link rel="stylesheet" href="estilo/dashboard.css">
		<link rel="stylesheet" href="estilo/superslides.css">
		<link rel="stylesheet" href="estilo/nprogress.css">
        <script type="text/javascript" src="scripts/jquery-ui.js"></script>
		<script src="scripts/nprogress.js"></script>
		<script src="scripts/exporting.js"></script>	
		
		<script src="scripts/jquery.animate-enhanced.min.js"></script>
		<script src="scripts/jquery.superslides.js" type="text/javascript" charset="utf-8"></script>	
		<script src="scripts/exporting.js"></script>
		<script src="scripts/data.js"></script>
		<script src="scripts/drilldown.js"></script>
		
		<!-- bootstrap table -->
		<link  rel="stylesheet" href="estilo/bootstrap-table.css">
		<script src="scripts/bootstrap-table.js"></script>
		<script src="scripts/bootstrap-table-pt-BR.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$().alert();
				
				if($("#alerta strong").html() == ""){
					$("#alerta").hide();
				}
				
				var urlAjax = "/monitoramento-servlet/upload";
				
				//configurando a modal
				$.ajax({
				  method: "GET",
				  url: urlAjax,
				  'async': false,
				  data: { act: "listar" },
				  dataType: "json"
				}).success(function( retorno ) {
					$('#tabelaAtendimento').bootstrapTable('load',retorno );
				});
			});
			
			
		</script>		
    </head>
 
    <body>
	    <nav class="navbar navbar-inverse navbar-fixed-top">
	      <div class="container">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="upload.jsp">Sistema de Monitoramento</a>
	        </div>
	        <div id="navbar" class="collapse navbar-collapse">
	          <ul class="nav navbar-nav">
	            <li><a href="dashboard/index.jsp">Dashboard</a></li>
	            <li class="active"><a href="upload.jsp">Importação de Xls</a></li>
	          </ul>
	        </div><!--/.nav-collapse -->
	      </div>
	    </nav>
	
	    <div class="container" style="margin-top: 6%;">
	    	<div class="alert alert-success alert-dismissible" role="alert" id="alerta">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>${message}</strong>
			</div>
	    	<div style="text-align: center;">
	            <h1>Importação de Xls</h1>
	    	</div>
	    	
            <form action="upload" method="post" enctype="multipart/form-data">
            	<input type="hidden" id="act" value="upload" />
            	<div style="text-align: center;">
            		<div style="margin-left: 37%; margin-bottom: 1%">
		                <input type="file" name="file" />
            		</div>
            		<div>
		                <input class="btn btn-primary" type="submit" value="Upload" />
            		</div>
            	</div>
            </form>      
            <div style="background-color: #F2F2F2; margin-top: 10px; border: 1px solid #FFFFFF; border-radius: 8px;">
	            <table id="tabelaAtendimento" data-toggle="table" 
				   data-classes="table table-hover"
				   data-sort-name="datacarga"
				   data-sort-order="desc"
				   data-height=410
				   data-striped="true"
				   data-page-size="7"
				   data-pagination="true"
				   data-search="true">
					<thead>
						<tr>
							<th data-sortable="true" data-field="datacarga">Data da Carga</th>
						</tr>
					</thead>
				</table>    
            </div>
	    </div><!-- /.container -->
  </body>
</html>