<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" charset="text/html; UTF-8">
        <title>Dashboard Universa</title>
        <script type="text/javascript" src="../scripts/jquery-1.9.1.js"></script>	
		<script src="../scripts/jquery.easing.1.3.js"></script>		
		<!-- Bootstrap -->
		<link rel="stylesheet" href="../estilo/bootstrap.min.css">
		<script src="../scripts/bootstrap.min.js"></script>
		<script src="../scripts/highcharts.js"></script>
		<script src="../scripts/highcharts-more.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Archivo+Narrow:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="../estilo/jquery-ui.css">
        <link rel="stylesheet" href="../estilo/dashboard.css">
		<link rel="stylesheet" href="../estilo/superslides.css">
		<link rel="stylesheet" href="../estilo/nprogress.css">
        <script type="text/javascript" src="../scripts/jquery-ui.js"></script>
		<script src="../scripts/nprogress.js"></script>
		<script src="../scripts/exporting.js"></script>	
		
		<script src="../scripts/jquery.animate-enhanced.min.js"></script>
		<script src="../scripts/jquery.superslides.js" type="text/javascript" charset="utf-8"></script>	
		<script src="../scripts/exporting.js"></script>
		<script src="../scripts/data.js"></script>
		<script src="../scripts/drilldown.js"></script>
		
		<!-- bootstrap table -->
		<link  rel="stylesheet" href="../estilo/bootstrap-table.css">
		<script src="../scripts/bootstrap-table.js"></script>
		<script src="../scripts/bootstrap-table-pt-BR.js"></script>

		<input type="hidden" id="paginaAtual" value="1" />
		<script type="text/javascript">
			var estados = Array();
			estados[2]  = Array('Distrito Federal','DF');
			estados[3]  = Array('Acre','AC');
			estados[4]  = Array('Amazonas','AM');
			estados[5]  = Array('Roraima','RR');
			estados[6]  = Array('Amapá','AP');
			estados[7]  = Array('Maranhão','MA');
			estados[8]  = Array('Rondônia','RO');
			estados[9]  = Array('Mato Grosso','MT');
			estados[10] = Array('Mato Grosso do Sul','MS');
			estados[11] = Array('Goiás','GO');
			estados[12] = Array('Paraná','PR');
			estados[13] = Array('São Paulo','SP');
			estados[14] = Array('Rio de Janeiro','RJ');
			estados[15] = Array('Espírito Santo','ES');
			estados[16] = Array('Minas Gerais','MG');
			estados[17] = Array('Santa Catarina','SC');
			estados[18] = Array('Rio Grande do Sul','RS');
			estados[19] = Array('Tocantins','TO');
			estados[20] = Array('Bahia','BA');
			estados[22] = Array('Sergipe','SE');
			estados[21] = Array('Alagoas','AL');
			estados[23] = Array('Piauí','PI');
			estados[24] = Array('Ceará','CE');
			estados[25] = Array('Rio Grande do Norte','RN');
			estados[26] = Array('Paraíba','PB');
			estados[27] = Array('Pernambuco','PE');
			estados[28] = Array('Pará','PA');

			var novoIntervalo;
			var quantidadeTelas = 2;
		
			var urlAjax = "/monitoramento-servlet/dashboard";
	
			var novoIntervalo;
			
			var quantidadeTelas = 2;
			
			$(document).ready(function(){
				carregarInformacoesPorEstado();
				
				$('#tela').load('tela1.jsp');
				
				NProgress.configure({ showSpinner: false, minimum: 0.01 });
				criarIntervalo();
			});
			
			$(document).keydown(function(e) {
				switch(e.which) {
					case 37: // left
						navegaTela('<');
					break;
					case 39: // right
						navegaTela('>')
					break;
					default: return; // exit this handler for other keys
				}
				e.preventDefault(); // prevent the default action (scroll / move caret)
			});
			
			function carregarInformacoesPorEstado(){
				//carregando as informacoes por estado
				$.ajax({
				  method: "POST",
				  url: urlAjax,
				  'async': false,
				  data: { act: "carregaEstados"},
				  dataType: "json"
				}).success(function( retorno ) {
					$.each(retorno, function( index, value ) {
						$.each(estados, function( indexEstado , valueEstado ) {
							if( valueEstado && valueEstado[1] == $.trim(value.estuf)){
								estados[indexEstado][3] = value.nota;
								estados[indexEstado][4] = value.tempo;
								estados[indexEstado][5] = value.qtd;
							}
						});
					});
				});
			}
			
			
			function criarIntervalo(){
				NProgress.start();
				
				var percentual = 0.00;
				NProgress.set(percentual);
				
				novoIntervalo = setInterval(function(){
					percentual = percentual + 0.01;
					NProgress.set(percentual);
					
					if(percentual >= 1){
						gerenciaTela( parseInt($('#paginaAtual').val()) + 1);
					}
				}, 120);
			}
			
			function navegaTela(direcao){
				var paginaAtual = $('#paginaAtual').val();
				
				if(direcao == '<'){
					gerenciaTela( parseInt(paginaAtual) - 1);
				}else if(direcao == '>'){
					gerenciaTela( parseInt(paginaAtual) + 1);
				} else {
					gerenciaTela('1');
				}
			}
			
			function gerenciaTela(tela){
				clearInterval(novoIntervalo);
				NProgress.done();
				
				if(tela < 1){
					tela = quantidadeTelas;
				}
				
				if(tela > quantidadeTelas){
					tela = '1';
				}	  
				  
				$("#tela").fadeOut( 300, function() {
					$('#tela').load('tela' + tela + '.jsp').fadeIn( 300 );
				});
				
				$('#paginaAtual').val(tela);

				$('[id^=linkTela]').removeClass('linkTelaAtual');
				$('#linkTela' + tela).addClass('linkTelaAtual');
				
				criarIntervalo();
			}
			
			function pausaSlide(){
				NProgress.done();
				clearInterval(novoIntervalo);
			}
			
			function retomaSlide(){
				criarIntervalo();
			}
			
		</script>		
    </head>
    <body>
		<div style="text-align:center;">
			<table style="width:100%;">
				<tr>
					<td rowspan="2" style="width:200px;">
						<div onclick="navegaTela('<')" class="iconeAnterior"></div>
					</td>
					<td colspan="2" style="text-align:center;" align="center">
						<span>
							<a href="#" id="linkTela1" class="linkTelaAtual" onclick="gerenciaTela('1')"> Monitoramento </a> |
							<a href="#" id="linkTela2" onclick="gerenciaTela('2')"> Mapa de Atendimentos </a>
						</span>			
					</td>
					<td rowspan="2" style="width:200px;">
						<div onclick="navegaTela('>')" class="iconeProximo"> </div>
					</td>					
				</tr>
				<tr>
					<td  style=" padding: 1px; padding-bottom: 5px; padding-top: 5px;"><div class="iconeStop" onclick="pausaSlide()"> </div></td>
					<td  style=" padding: 1px; padding-bottom: 5px; padding-top: 5px;"><div class="iconePlay" onclick="retomaSlide()"> </div></td>
				</tr>
			</table>			
		</div>			
		<div class="linha" style="margin-top: 0px;">&nbsp;</div>
			<div class="conteudo" id="tela"></div>
		</div>
    </body>
</html>