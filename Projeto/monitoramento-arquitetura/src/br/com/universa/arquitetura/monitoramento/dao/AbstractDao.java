package br.com.universa.arquitetura.monitoramento.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.universa.arquitetura.monitoramento.bean.AbstractBean;
import br.com.universa.arquitetura.monitoramento.jdbc.ConnectionFactory;
import br.com.universa.arquitetura.monitoramento.util.ArrayListUtil;

public abstract class AbstractDao<T extends AbstractBean> {

	private Connection connection;

	protected abstract String doGetNomeTabela();

	protected abstract String doGetNomeChavePrimaria();

	protected abstract String doGetNomeColunaStatus();

	protected abstract ArrayList<String> doGetCamposTabela();

	abstract protected T doPopularBeanDaLista(ResultSet rs);

	/**
	 * Configura o Statement para as SQL's O Statement deve ser preparado de
	 * acordo com a ordem dos Campos tabela <code>
	 *  //Array de Campos
	 * 	return ArrayListUtil.explode(",", "nome, descricao");
	 *  //Statement
	 *  preparedStatement.setString(1, bean.getNome());
	 * 	preparedStatement.setString(2, bean.getDescricao());
	 * </code>
	 * 
	 * @param preparedStatement
	 * @param bean
	 * @throws SQLException
	 */
	protected abstract void doConfigurarPreparedStatement(
			PreparedStatement preparedStatement, T bean) throws SQLException;

	/**
	 * Construtor da Classe, abre a conex�o com o banco.
	 * 
	 * @throws ClassNotFoundException
	 */
	public AbstractDao() throws ClassNotFoundException {
		connection = new ConnectionFactory().getConnection();
	}

	public T salvar(T bean) {
		if (bean.getId() == null) {
			bean = incluir(bean);
		} else {
			bean = alterar(bean);
		}

		return bean;
	}

	private T alterar(T bean) {
		String sql = "UPDATE " + doGetNomeTabela() + " SET "
				+ montarStatementsUpdate() + " WHERE "
				+ doGetNomeChavePrimaria() + " = ?";
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareStatement(sql,
					Statement.RETURN_GENERATED_KEYS);
			doConfigurarPreparedStatement(preparedStatement, bean);
			preparedStatement.setLong(doGetCamposTabela().size() + 1,
					bean.getId());

			preparedStatement.executeUpdate();

			ResultSet generatedKeys = preparedStatement.getGeneratedKeys();

			if (generatedKeys.next()) {
				bean.setId(generatedKeys.getLong(doGetNomeChavePrimaria()));
			}

//			connection.close();
			if(!connection.isClosed()){
				connection.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bean;
	}

	public void excluir(T bean) {
		String sql = "UPDATE " + doGetNomeTabela() + " SET "
				+ doGetNomeColunaStatus() + " = ? WHERE "
				+ doGetNomeChavePrimaria() + " = ?";

		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, "I");
			preparedStatement.setLong(2, bean.getId());

			preparedStatement.executeUpdate();
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private T incluir(T bean) {
		String sql = "INSERT INTO " + doGetNomeTabela() + "("
				+ ArrayListUtil.implode(", ", doGetCamposTabela()) + ", "
				+ doGetNomeColunaStatus() + ") values ("
				+ montarStatementsInsert() + ", ?)";

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(
					sql, Statement.RETURN_GENERATED_KEYS);
			doConfigurarPreparedStatement(preparedStatement, bean);
			preparedStatement.setString(doGetCamposTabela().size() + 1, "A");

			preparedStatement.executeUpdate();

			ResultSet generatedKeys = preparedStatement.getGeneratedKeys();

			if (generatedKeys.next()) {
				bean.setId(generatedKeys.getLong(doGetNomeChavePrimaria()));
			}

			connection.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bean;
	}

	/**
	 * M�todo de lista default. � necess�rio sempre uma inst�ncia de ArrayList,
	 * mesmo que vazio
	 * 
	 * @param where
	 * @return
	 * @throws SQLException
	 */
	public List<T> listar(ArrayList<String> where) throws SQLException {
		if(doGetNomeColunaStatus() != null){
			where.add(doGetNomeColunaStatus() + " = 'A'");
		} else {
			where.add("TRUE");
		}

		String sql = "SELECT * FROM " + doGetNomeTabela() + " WHERE "
				+ ArrayListUtil.implode(" AND ", where) + ";";

		PreparedStatement stmt = this.connection.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		List<T> beans = new ArrayList<T>();
		while (rs.next()) {
			T bean = doPopularBeanDaLista(rs);
			beans.add(bean);
		}
		rs.close();
		stmt.close();
		
		if(!connection.isClosed()){
			connection.close();
		}
		
		return beans;
	}

	private String montarStatementsInsert() {
		ArrayList<String> strings = new ArrayList<String>();

		for (int i = 0; i < doGetCamposTabela().size(); i++) {
			strings.add("?");
		}

		return ArrayListUtil.implode(", ", strings);
	}

	private String montarStatementsUpdate() {
		ArrayList<String> strings = new ArrayList<String>();

		for (int i = 0; i < doGetCamposTabela().size(); i++) {
			strings.add(doGetCamposTabela().get(i) + " = ?");
		}

		return ArrayListUtil.implode(", ", strings);
	}

	public Connection getConnection() {
		return connection;
	}

}
