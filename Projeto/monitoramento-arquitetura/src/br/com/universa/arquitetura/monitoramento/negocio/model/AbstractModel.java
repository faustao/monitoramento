package br.com.universa.arquitetura.monitoramento.negocio.model;

import java.sql.SQLException;
import java.util.ArrayList;

import br.com.universa.arquitetura.monitoramento.bean.AbstractBean;
import br.com.universa.arquitetura.monitoramento.dao.AbstractDao;

@SuppressWarnings("rawtypes")
public abstract class AbstractModel<T extends AbstractBean, I extends AbstractDao> {
	
	private I dao;

	protected I getDao() {
		if (dao == null) {
			dao = doGetDao();
		}

		return dao;
	}

	abstract protected I doGetDao();
	
	@SuppressWarnings("unchecked")
	public ArrayList<T> listar(ArrayList<String> where) {
		try {
			return (ArrayList<T>) getDao().listar(where);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
