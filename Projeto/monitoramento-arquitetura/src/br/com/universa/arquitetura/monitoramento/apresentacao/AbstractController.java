package br.com.universa.arquitetura.monitoramento.apresentacao;

import javax.servlet.http.HttpServlet;

import br.com.universa.arquitetura.monitoramento.bean.AbstractBean;
import br.com.universa.arquitetura.monitoramento.negocio.facade.AbstractFacade;

@SuppressWarnings("rawtypes")
public abstract class AbstractController<T extends AbstractBean, I extends AbstractFacade>
		extends HttpServlet {

	private static final long serialVersionUID = 6928394650985758569L;

	private I facade;

	private T bean;

	protected I getFacade() {
		if (facade == null) {
			facade = doGetFacade();
		}

		return facade;
	}

	protected T getBean() {
		if (bean == null) {
			bean = doGetBean();
		}

		return bean;
	}

	abstract protected T doGetBean();

	abstract protected I doGetFacade();
}
