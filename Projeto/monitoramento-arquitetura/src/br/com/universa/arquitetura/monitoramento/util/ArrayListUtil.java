package br.com.universa.arquitetura.monitoramento.util;

import java.util.ArrayList;

public class ArrayListUtil {
	public static String implode(String glue, ArrayList<String> pieces) {
		String result = "";

		if (!pieces.isEmpty()) {
			StringBuilder builder = new StringBuilder();
			builder.append(pieces.remove(0));
			
			for (String piece : pieces) {
				builder.append(glue);
				builder.append(piece);
			}
			
			result = builder.toString();
		}

		return result;
	}
	
	public static ArrayList<String> explode(String delimiter, String string){
		ArrayList<String> arListStrings = new ArrayList<String>();
		String[] strings = string.split(delimiter);
		
		for (int i = 0; i < strings.length; i++) {
			arListStrings.add(strings[i]);
		}
		
		return arListStrings;
	}
}
