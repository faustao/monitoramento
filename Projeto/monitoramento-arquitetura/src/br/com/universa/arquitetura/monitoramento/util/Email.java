package br.com.universa.arquitetura.monitoramento.util;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Email {
	private static String hostSmtp = "smtp.gmail.com";
	private static String emailRemetente = "no.reply.sismonitoramento@gmail.com";
	private static String senhaRemetente = "15Universa";
	/**
	 * Destinat�rios separados por virgula, quando houver mais de um. Ex.:
	 * <code>
	 *  Email.setDestinatarios("email1@java.com, email2@java.com, email3@java.com");
	 * </code>
	 */
	private String destinatarios;
	private String assunto;
	private String conteudo;

	public void enviarEmail() throws AddressException, MessagingException {
		Properties props = new Properties();
		/** Par�metros de conex�o com servidor Gmail */
		props.put("mail.smtp.host", hostSmtp);
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(emailRemetente,
								senhaRemetente);
					}
				});

		/** Ativa Debug para sess�o */
		session.setDebug(true);

		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(emailRemetente)); // Remetente

		// Destinat�rio(s) separados por virgula
		Address[] toUser = InternetAddress.parse(destinatarios);

		message.setRecipients(Message.RecipientType.TO, toUser);
		message.setSubject(assunto);// Assunto
		message.setText(conteudo);
		/** M�todo para enviar a mensagem criada */
		Transport.send(message);

	}

	public String getDestinatarios() {
		return destinatarios;
	}

	public void setDestinatarios(String destinatarios) {
		this.destinatarios = destinatarios;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}
}
