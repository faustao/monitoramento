package br.com.universa.arquitetura.monitoramento.bean;

public class AbstractBean {
	private Long id;
	
	private String status;

	public Long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
