package br.com.universa.arquitetura.monitoramento.negocio.facade;

import java.util.ArrayList;

import br.com.universa.arquitetura.monitoramento.bean.AbstractBean;
import br.com.universa.arquitetura.monitoramento.negocio.model.AbstractModel;

@SuppressWarnings("rawtypes")
public abstract class AbstractFacade<T extends AbstractBean, I extends AbstractModel> {
	
	private I model;

	protected I getModel() {
		if (model == null) {
			model = doGetModel();
		}

		return model;
	}

	abstract protected I doGetModel();
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<T> listar(ArrayList<String> where) {
		return getModel().listar(where);
	}
}
