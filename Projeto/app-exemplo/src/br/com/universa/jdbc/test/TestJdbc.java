package br.com.universa.jdbc.test;

import java.sql.Connection;
import java.sql.SQLException;

import br.com.universa.arquitetura.monitoramento.jdbc.ConnectionFactory;


public class TestJdbc {
	public static void main(String[] args) throws SQLException, ClassNotFoundException{
		Connection connection = new ConnectionFactory().getConnection();
		System.out.println("Conex�o aberta!");
		connection.close();
	}
}
