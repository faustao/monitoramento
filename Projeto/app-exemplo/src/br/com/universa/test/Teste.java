package br.com.universa.test;

import br.com.universa.bean.TesteBean;
import br.com.universa.dao.TesteDao;

public class Teste {

	public static void main(String[] args) throws ClassNotFoundException {
		TesteBean bean = new TesteBean();
		bean.setNome("Teste Nome");
		bean.setDescricao("Teste Descri��o");
		
		TesteDao dao = new TesteDao();
		bean = dao.salvar(bean);
		
		System.out.println("Incluir");
		System.out.println(bean.getId());
		
		dao = new TesteDao();
		System.out.println("Alterar");
		bean.setNome("Teste Nome 1");
		bean.setDescricao("Teste Descri��o 1");
		
		bean = dao.salvar(bean);
		
		dao = new TesteDao();
		dao.excluir(bean);
		
		System.out.println(bean.getId());

	}

}
