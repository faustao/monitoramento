package br.com.universa.bean;

import br.com.universa.arquitetura.monitoramento.bean.AbstractBean;

public class TesteBean extends AbstractBean {
	
	private String nome;
	
	private String descricao;

	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
