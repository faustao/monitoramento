package br.com.universa.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.universa.arquitetura.monitoramento.dao.AbstractDao;
import br.com.universa.arquitetura.monitoramento.util.ArrayListUtil;
import br.com.universa.bean.TesteBean;

public class TesteDao extends AbstractDao<TesteBean> {

	public TesteDao() throws ClassNotFoundException {
		super();
	}

	@Override
	protected void doConfigurarPreparedStatement(
			PreparedStatement preparedStatement, TesteBean bean)
			throws SQLException {
		preparedStatement.setString(1, bean.getNome());
		preparedStatement.setString(2, bean.getDescricao());
	}

	@Override
	protected String doGetNomeTabela() {
		return "tb_teste";
	}

	@Override
	protected String doGetNomeChavePrimaria() {
		return "id";
	}

	@Override
	protected String doGetNomeColunaStatus() {
		return "status";
	}

	@Override
	protected ArrayList<String> doGetCamposTabela() {
		return ArrayListUtil.explode(",", "nome, descricao");
	}

}
